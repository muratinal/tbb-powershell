# Question-1 :

List the services running with PowerShell.

# Question-2 :

List the services whose Startup Type is Automatic and Manual separately.

# Question-3 :

Run services whose Startup Type is Automatic (Delayed Start) with a single command.

# Question-4 :

Write the script that reports the disk sizes in html format and sends the report to the mail.

# Question-5 :

Perform Post and Get requests on a web api with Powershell.

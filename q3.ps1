﻿Get-Service | 
    Where-Object {$PSItem.StartType -like "Automatic" -and $PSItem.Status -like "Stopped"} | 
        Start-Service -ErrorAction SilentlyContinue
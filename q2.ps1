﻿Write-Host "Automatic Services" 
Get-Service | Where-Object {$PSItem.StartType -like "Automatic"} | Format-Table -Property Name,DisplayName,StartType

Write-Host "Manual Services"
Get-Service | Where-Object {$PSItem.StartType -like "Manual"} | Format-Table -Property Name,DisplayName,StartType